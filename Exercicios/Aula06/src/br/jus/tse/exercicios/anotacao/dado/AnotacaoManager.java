package br.jus.tse.exercicios.anotacao.dado;

import java.lang.annotation.Annotation;

public class AnotacaoManager {
	
	public int recuperarValorViciadoDado() {
		Class classe = Dado.class;
		Annotation[] annotations = classe.getAnnotations();
		
		for(Annotation annotation : annotations) {
			if(annotation instanceof AnotacaoDado) {
				AnotacaoDado anotacaoDado = (AnotacaoDado) annotation;
				return anotacaoDado.valorViciado();
			}
		}
		return 0;
	}
	
}
