package br.jus.tse.exercicios.anotacao.dado;

import java.util.Random;

@AnotacaoDado(valorViciado=6)
public class Dado {
	
	private int valorViciado;
	private int qtdJogadas;
	private int tamanhoDado;
	
	public int getValorViciado() {
		return valorViciado;
	}
	
	public void setValorViciado(int valorViciado) {
		this.valorViciado = valorViciado;
	}
	
	public int getQtdJogadas() {
		return qtdJogadas;
	}

	public void setQtdJogadas(int qtdJogadas) {
		this.qtdJogadas = qtdJogadas;
	}
	
	public int getTamanhoDado() {
		return tamanhoDado;
	}

	public void setTamanhoDado(int tamanhoDado) {
		this.tamanhoDado = tamanhoDado;
	}

	public Dado(int lados, int jogadas) {
		this.setTamanhoDado(lados);
		this.setQtdJogadas(jogadas);
		
		AnotacaoManager anotacaoManager = new AnotacaoManager();
		this.setValorViciado(anotacaoManager.recuperarValorViciadoDado());
	}
	
	//4/6 = 66,66%
	public void jogarDado() {
		
		//(66,66666667 * 100) / qtd = 4/6
		int porcentagemVicio = 67;
		int qtdVicio = (porcentagemVicio * getQtdJogadas()) / 100;
		
		int contadorVicio = 0;
		int impressoes = 0;
		
		while(impressoes < getQtdJogadas()) {
			int valorAleatorio = randomizar();
			
			if(getQtdJogadas() < 2) {
				System.out.println("Valor sorteado: " + getValorViciado());
				return;
			}
			
			if(contadorVicio < qtdVicio) {
				System.out.println("Valor sorteado: " + getValorViciado());
				contadorVicio++;
				impressoes++;
			} else if(valorAleatorio != getValorViciado()) {
				System.out.println("Valor sorteado: " + valorAleatorio);
				impressoes++;
			}
		}
	}
	
	private int randomizar() {
		Random random = new Random();
		int minimo = 1;
		int maximo = getTamanhoDado();
		
		return random.nextInt((maximo - minimo) + 1) + minimo;
	}
	
}
