package br.jus.tse.exercicios.anotacao.dado;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	
	public static void solicitarTamanhoDado() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Informe o tamanho do dado (qtd lados): ");
        try{
            int lados = Integer.parseInt(br.readLine());
    		solicitarQuantidade(lados);
        }catch(NumberFormatException nfe){
            System.err.println("Formato inválido! Digite somente números.");
            solicitarTamanhoDado();
        }
	}
	
	public static void solicitarQuantidade(int lados) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Informe o número de vezes que deseja lançar o dado: ");
        try{
            int jogadas = Integer.parseInt(br.readLine());
    		Dado dado = new Dado(lados, jogadas);
    		dado.jogarDado();
        }catch(NumberFormatException nfe){
            System.err.println("Formato inválido! Digite somente números.");
            solicitarQuantidade(lados);
        }
	}
	
	public static void main(String[] args) { 
        try {
			solicitarTamanhoDado();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
