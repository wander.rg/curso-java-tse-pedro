package br.jus.tse.exercicios.arvorebinaria;

public class Ramo<T extends Number> {

	private Ramo<T> ramoEsquerdo;
	
	private Ramo<T> ramoDireito;

	private T valor;
	
	public Ramo<T> getRamoEsquerdo() {
		return ramoEsquerdo;
	}
	
	public void setRamoEsquerdo(Ramo<T> ramoEsquerdo) {
		this.ramoEsquerdo = ramoEsquerdo;
	}

	public Ramo<T> getRamoDireito() {
		return ramoDireito;
	}

	public void setRamoDireito(Ramo<T> ramoDireito) {
		this.ramoDireito = ramoDireito;
	}

	public T getValor() {
		return valor;
	}

	public void setValor(T valor) {
		this.valor = valor;
	}

	public Ramo(T valor) {
		setValor(valor);
	}
	
}
