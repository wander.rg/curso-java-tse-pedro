package br.jus.tse.exercicios.arvorebinaria;

public class Arvore<T extends Number> {
	
	private int qtdNos;
	
	private Ramo<T> ramoRaiz;
	
	public int getQtdNos() {
		return qtdNos;
	}

	public void setQtdNos(int qtdNos) {
		this.qtdNos = qtdNos;
	}

	public Ramo<T> getRamoRaiz() {
		return ramoRaiz;
	}

	public void setRamoRaiz(Ramo<T> ramoRaiz) {
		this.ramoRaiz = ramoRaiz;
	}

	public Ramo<T> ArvoreBinaria(T valor) {
		return this.adicionarRamo(new Ramo<T>(valor), getRamoRaiz());
	}
	
	public Ramo<T> adicionarRamo(Ramo<T> novoRamo, Ramo<T> ramoAnterior) {
		if (getRamoRaiz() == null) {
			setRamoRaiz(novoRamo);
			return novoRamo;
		}
		
		if (ramoAnterior != null) {
			if(comparar(novoRamo.getValor(), ramoAnterior.getValor()) <= 0) {
				ramoAnterior.setRamoEsquerdo(this.adicionarRamo(novoRamo, ramoAnterior.getRamoEsquerdo()));
			} else if (comparar(novoRamo.getValor(), ramoAnterior.getValor()) > 0) {
				ramoAnterior.setRamoDireito(this.adicionarRamo(novoRamo, ramoAnterior.getRamoDireito()));
			} else {
				return null;
			}
		} else {
			ramoAnterior = novoRamo;
		}
		
		return ramoAnterior;
		
	}
	
	public int comparar(T novoValor, T valorAnterior) {
		return ((Comparable)novoValor).compareTo((Comparable)valorAnterior);
	}
	
	public void procurarRamo() {
		
	}

}
