package br.jus.tse.exercicio6;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class ArquivoManager {
	
	public void escreverNoArquivo(String texto) throws IOException {
		
		FileWriter arquivo = new FileWriter("arquivo/arquivo.txt");
	    PrintWriter gravar = new PrintWriter(arquivo);
	    
	    gravar.printf(texto);
	    
	    arquivo.close();
	}

}
