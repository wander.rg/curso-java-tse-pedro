package br.jus.tse.exercicio6;

import java.io.IOException;

import javax.swing.JOptionPane;

public class Main {
	
	private static String titulo = "GRAVAR EM ARQUIVO!";
	
	private static void solicitarDados() throws IOException {
		try {
			String pergunta = "Digite uma frase abaixo:";
			
			String texto = JOptionPane.showInputDialog(null, pergunta, titulo, JOptionPane.QUESTION_MESSAGE);
			
			// Verificar se o usuário fechou ou cancelou
			if(texto == null) {
				return;
			}
			
			ArquivoManager arquivoManager = new ArquivoManager();
			arquivoManager.escreverNoArquivo(texto);
			
		} catch (NumberFormatException e) {
			solicitarDados();
		}
	}

	public static void main(String[] args) {
		try {
			solicitarDados();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
