package br.jus.tse.exercicio3;

import java.io.IOException;

import javax.swing.JOptionPane;

public class Main {
	
	private static String titulo = "RAIZ QUADRADA";
	
	private static void solicitarDados() throws IOException {
		try {
			String pergunta = "Digite um valor: ";
			
			String input = JOptionPane.showInputDialog(null, pergunta, titulo, JOptionPane.QUESTION_MESSAGE);
			
			// Verificar se o usuário fechou ou cancelou
			if(input == null) {
				return;
			}
			
			double numero = Double.parseDouble(input);
			
			if(numero < 0) {
				solicitarDados();
			}
			
			double raiz = Math.sqrt(numero);
			
			String msg = String.format("A raiz quadrada do número %.2f é: %.2f", numero, raiz);
			JOptionPane.showMessageDialog(null, msg);
		} catch (NumberFormatException e) {
			solicitarDados();
		}
	}
	
	public static void main(String[] args) {
		try {
			solicitarDados();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
