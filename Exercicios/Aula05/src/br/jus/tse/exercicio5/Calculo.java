package br.jus.tse.exercicio5;

public class Calculo {
	
	public int retornaValorMaior(int[] valores) {
		Integer maior = null;
		
		for (int n : valores) {
			if(maior == null) {
				maior = n;
				continue;
			}
			
			if(n > maior) {
				maior = n;
			}
		}
		
		return maior;
	}
	
	public int retornaValorMenor(int[] valores) {
		Integer menor = null;
		
		for (int n : valores) {
			if(menor == null) {
				menor = n;
				continue;
			}
			
			if(n < menor) {
				menor = n;
			}
		}
		
		return menor;
	}

}
