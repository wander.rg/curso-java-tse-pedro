package br.jus.tse.exercicio5;

import java.io.IOException;

import javax.swing.JOptionPane;

public class Main {
	
	private static final int MAX_INPUTS = 5;
	
	private static String titulo = "VALOR MAIOR E MENOR!";
	
	private static void solicitarDados() throws IOException {
		try {
			int[] numeros = new int[MAX_INPUTS];
			
			for(int i=0; i < MAX_INPUTS; i++) {
				String pergunta = String.format("Digite o %dº valor: ", i + 1);
				
				String input = JOptionPane.showInputDialog(null, pergunta, titulo, JOptionPane.QUESTION_MESSAGE);
				
				// Verificar se o usuário fechou ou cancelou
				if(input == null) {
					return;
				}
				
				int n = Integer.parseInt(input);
				
				numeros[i] = n;
			}
			
			Calculo calculo = new Calculo();
			
			int maior = calculo.retornaValorMaior(numeros);
			int menor = calculo.retornaValorMenor(numeros);
			
			String msg = "Números informados: ";
			
			for (int n : numeros) {
				msg += n + " ";
			}
			msg += "\n";
			
			msg += String.format("Maior número: %d\n", maior);			
			msg += String.format("Menor número: %d\n", menor);	
			
			JOptionPane.showMessageDialog(null, msg);
		} catch (NumberFormatException e) {
			solicitarDados();
		}
	}

	public static void main(String[] args) {
		try {
			solicitarDados();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
