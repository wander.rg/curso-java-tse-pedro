package br.jus.tse.exercicio1.joptionpane;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.jus.tse.exercicio1.Pessoa;

public class Main {
	
	private static final int QTD_PESSOAS = 3;
	
	private static String titulo = "PESSOA MAIS VELHA!";
	
	private static void solicitarDados() throws IOException {
		try {
			List<Pessoa> pessoas = new ArrayList<Pessoa>();
			
			for(int i = 1; i <= QTD_PESSOAS; i++) {
				
				String msgNome = String.format("Digite o nome da %dª pessoa: \n", i);
				
				String msgIdade = String.format("Digite a idade da %dª pessoa: \n", i);
				
				String nome = JOptionPane.showInputDialog(null, msgNome, titulo, JOptionPane.QUESTION_MESSAGE);
				
				// Verificar se o usuário fechou ou cancelou
				if(nome == null) {
					return;
				}
				
				String inputIdade = JOptionPane.showInputDialog(null, msgIdade, titulo, JOptionPane.QUESTION_MESSAGE);
				
				// Verificar se o usuário fechou ou cancelou
				if(inputIdade == null) {
					return;
				}
				
				int idade = Integer.parseInt(inputIdade);
				
				Pessoa pessoa = new Pessoa(nome, idade);
				
				pessoas.add(pessoa);
			}
			
			Pessoa maisVelha = null;
			List<Integer> empate = new ArrayList<Integer>();
			
			for(int j = 0; j < pessoas.size(); j++) {
				Pessoa pessoa = pessoas.get(j);
				
				if(maisVelha == null) {
					maisVelha = pessoa;
					empate.add(j);
					continue;
				}
				
				if(pessoa.getIdade() == maisVelha.getIdade()) {
					empate.add(j);
					continue;
				}
				
				if(pessoa.getIdade() > maisVelha.getIdade()) {
					maisVelha = pessoa;
					empate.clear();
					empate.add(j);
				}
			}
			
			if(empate.size() > 1) {
				String pessoasEmpate = "";
				
				for (Integer index : empate) {
					pessoasEmpate += pessoas.get(index).getNome() + "; ";
				}
				
				String msg = String.format("Deu empate! As pessoas mais velhas são: %s", pessoasEmpate);
				
				JOptionPane.showMessageDialog(null, msg);
				
				return;
			}
			
			String msg = String.format("A pessoa mais velha é: %s", maisVelha.getNome());
			
			JOptionPane.showMessageDialog(null, msg);
		} catch (NumberFormatException e) {
			solicitarDados();
		}
	}

	public static void main(String[] args) {
		try {
			solicitarDados();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
