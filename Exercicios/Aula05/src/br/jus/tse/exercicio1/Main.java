package br.jus.tse.exercicio1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
	
	private static final int QTD_PESSOAS = 3;
	private static Scanner scanner;
	
	private static void solicitarDados() throws IOException {
		try {
			List<Pessoa> pessoas = new ArrayList<Pessoa>();
			
			for(int i = 1; i <= QTD_PESSOAS; i++) {
				scanner = new Scanner(System.in);
				
				System.out.printf("Digite o nome da %dª pessoa: \n", i);
				String nome = scanner.next();
				
				System.out.printf("Digite a idade da %dª pessoa: \n", i);
				Integer idade = scanner.nextInt();
				
				Pessoa pessoa = new Pessoa(nome, idade);
				
				pessoas.add(pessoa);
			}
			
			Pessoa maisVelha = null;
			List<Integer> empate = new ArrayList<Integer>();
			
			for(int j = 0; j < pessoas.size(); j++) {
				Pessoa pessoa = pessoas.get(j);
				
				if(maisVelha == null) {
					maisVelha = pessoa;
					empate.add(j);
					continue;
				}
				
				if(pessoa.getIdade() == maisVelha.getIdade()) {
					empate.add(j);
					continue;
				}
				
				if(pessoa.getIdade() > maisVelha.getIdade()) {
					maisVelha = pessoa;
					empate.clear();
					empate.add(j);
				}
			}
			
			if(empate.size() > 1) {
				String pessoasEmpate = "";
				
				for (Integer index : empate) {
					pessoasEmpate += pessoas.get(index).getNome() + "; ";
				}
				
				System.out.printf("Deu empate! As pessoas mais velhas são: %s", pessoasEmpate);
				
				return;
			}
			
			System.out.println("\n=================");
			System.out.printf("A pessoa mais velha é: %s", maisVelha.getNome());
		} catch (InputMismatchException e) {
			solicitarDados();
		}	
	}

	public static void main(String[] args) {
		System.out.println("=================");
		System.out.println("PESSOA MAIS VELHA");
		System.out.println("=================");
		
		try {
			solicitarDados();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
