package br.jus.tse.exercicio2.joptionpane;

import java.io.IOException;

import javax.swing.JOptionPane;

public class Main {
	
	private static final double P_DISTRIBUIDOR = 0.28; //28%
	private static final double P_IMPOSTOS = 0.45; //45%
	
	private static String titulo = "CARRO - CUSTO";
	
	private static void solicitarDados() throws IOException {
		try {
			String pergunta = "Digite o custo de produção do carro: ";
			
			String input = JOptionPane.showInputDialog(null, pergunta, titulo, JOptionPane.QUESTION_MESSAGE);
			
			// Verificar se o usuário fechou ou cancelou
			if(input == null) {
				return;
			}
			
			double valorFabricacao = Double.parseDouble(input);
			
			double lucroDistruibuidor = valorFabricacao * P_DISTRIBUIDOR;
			double lucroGoverno = valorFabricacao * P_IMPOSTOS;
			double valorTotal = valorFabricacao + lucroDistruibuidor + lucroGoverno;
			
			String msg = String.format("Valor total do custo para o consumidor: %.2f", valorTotal);
			JOptionPane.showMessageDialog(null, msg);
		} catch (NumberFormatException e) {
			solicitarDados();
		}
	}
	
	public static void main(String[] args) {
		try {
			solicitarDados();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
