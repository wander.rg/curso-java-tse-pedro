package br.jus.tse.exercicio2;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
	
	private static final double P_DISTRIBUIDOR = 0.28; //28%
	private static final double P_IMPOSTOS = 0.45; //45%
	private static Scanner scanner;
	
	private static void solicitarDados() throws IOException {
		try {
			scanner = new Scanner(System.in);
			
			System.out.println("Digite o custo de produção do carro: ");
			double valorFabricacao = scanner.nextDouble();
			
			double lucroDistruibuidor = valorFabricacao * P_DISTRIBUIDOR;
			double lucroGoverno = valorFabricacao * P_IMPOSTOS;
			double valorTotal = valorFabricacao + lucroDistruibuidor + lucroGoverno;
			
			System.out.printf("Valor total do custo para o consumidor: %.2f", valorTotal);
		} catch (InputMismatchException e) {
			// TODO: handle exception
		}
	}
	
	public static void main(String[] args) {
		System.out.println("=============");
		System.out.println("CARRO - CUSTO");
		System.out.println("=============");
		
		try {
			solicitarDados();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
