package br.jus.tse.exercicio4;

import java.util.ArrayList;
import java.util.List;

public class Bingo {
	
	private int rodadas = 5;
	
	private List<Integer> resultado;
	
	public int getRodadas() {
		return rodadas;
	}
	
	public List<Integer> getResultado() {
		return resultado;
	}

	public void sortear() {
		List<Integer> numeros = new ArrayList<Integer>();
		
		int contador = 0;
		
		while(contador < rodadas) {
			int r = random();
			
			if(!numeros.contains(r)) {
				numeros.add(r);
				contador++;
			}
		}
		
		this.resultado = numeros;
	}
	
	private int random() {
		return (int)(Math.random() * 99) + 1;
	}
	
	public boolean verificarResultado(List<Integer> cartela) {	
		boolean ganhou = false;
		
		for (Integer n : cartela) {
			if(this.resultado.contains(n)) {
				ganhou = true;
			} else {
				ganhou = false;
				break;
			}
		}
		
		return ganhou;
	}
}
