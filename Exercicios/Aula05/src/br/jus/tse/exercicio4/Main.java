package br.jus.tse.exercicio4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class Main {
	
	private static String titulo = "BINGO!";
	
	private static void solicitarDados() throws IOException {
		try {
			
			List<Integer> cartela = new ArrayList<Integer>();
			
			Bingo bingo = new Bingo();
			
			int rodadas = bingo.getRodadas();
			
			int contador = 0;
			
			while(contador < rodadas) {
				String pergunta = String.format("Digite o %dº valor: ", contador + 1);
				
				String input = JOptionPane.showInputDialog(null, pergunta, titulo, JOptionPane.QUESTION_MESSAGE);
				
				// Verificar se o usuário fechou ou cancelou
				if(input == null) {
					return;
				}
				
				int numero = Integer.parseInt(input);
				
				if(!cartela.contains(numero)) {
					cartela.add(numero);
					contador++;
				}
			}
			
			bingo.sortear();
			
			String msg = "Não ganhou! \n";
			
			if(bingo.verificarResultado(cartela)) {
				msg = "Você ganhou! \n";
			}
			
			String resultado = "Resultado: ";
			
			for (Integer n : bingo.getResultado()) {
				resultado += n + " ";
			}
			resultado += "\n";
			
			String cartelaJogador = "Seu jogo: ";
			
			for (Integer n : cartela) {
				cartelaJogador += n + " ";
			}
			cartelaJogador += "\n";
			
			msg += resultado + cartelaJogador;
			
			JOptionPane.showMessageDialog(null, msg);			
		} catch (NumberFormatException e) {
			solicitarDados();
		}
	}

	public static void main(String[] args) {
		try {
			solicitarDados();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
