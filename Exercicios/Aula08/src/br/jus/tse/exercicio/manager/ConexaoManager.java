package br.jus.tse.exercicio.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoManager {

	private static final String URL_JDBC = "jdbc:postgresql://localhost:5432/endereco";
	
	private static final String LOGIN = "postgres";
	
	private static final String SENHA = "";
	
	private Connection connection;
	
	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public ConexaoManager() throws ClassNotFoundException {
		Class.forName("org.postgresql.Driver");
	}
	
	public void abrirConexao() throws SQLException {
		setConnection(DriverManager.getConnection(URL_JDBC, LOGIN, SENHA));
	}
	
	public void fecharConexao() throws SQLException {
		getConnection().close();
	}
	
}
