package br.jus.tse.exercicio.manager;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class EnderecoManager {
	
	private ConexaoManager conexaoManager;
	
	public List<String> obterLocalidadesPorUF(String uf) {
		
		try {
			this.conexaoManager = new ConexaoManager();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		
		try {
			this.conexaoManager.abrirConexao();
			
			String query = "SELECT loc.no_localidade FROM endereco.uf as uf LEFT JOIN endereco.localidade as loc ON loc.nu_uf = uf.nu_uf WHERE uf.sg_uf ILIKE ?";
			
			PreparedStatement statement = this.conexaoManager.getConnection().prepareStatement(query);
			statement.setString(1, uf.toUpperCase());
			
			ResultSet resultSet = statement.executeQuery();
			
			List<String> localidades = new ArrayList<String>();
			
			while(resultSet.next()) {
				localidades.add(resultSet.getString(1));
			}
			
			statement.close();
			this.conexaoManager.fecharConexao();
			
			return localidades;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
}
