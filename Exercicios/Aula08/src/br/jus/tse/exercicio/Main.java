package br.jus.tse.exercicio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import br.jus.tse.exercicio.manager.EnderecoManager;

public class Main {
	
	public static void solicitarUF() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Informe a sigla da UF: ");
        try{
        	EnderecoManager enderecoManager = new EnderecoManager();
        	List<String> localidades = enderecoManager.obterLocalidadesPorUF(br.readLine());
        	if(localidades != null && localidades.size() > 0) {
        		System.out.println("Quantidades de localidades encontradas: " + localidades.size());
        		
        		System.out.println("====== Localidades ======");
        		
        		for (String string : localidades) {
					System.out.println(string);
				}
        	} else {
        		System.out.println("==============================");
        		System.out.println("Nenhuma localidade encontrada!");
        		System.out.println("==============================");
        	}
        }catch(NumberFormatException nfe){
            System.err.println("Formato inválido! Digite a sigla da UF corretamente.");
            solicitarUF();
        }
	}
	
	public static void main(String[] args) { 
        try {
			solicitarUF();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
