package br.jus.tse.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Reflection {
	
	public void imprimirObjeto(Object object) {
		imprimirClasse(object.getClass());
	}
	
	private void imprimirClasse(Class classe) {
		if (classe != null) {
			System.out.println("====================");
			System.out.println("       CLASSE       ");
			System.out.println("====================");
			
			System.out.println(classe);
			
			imprimirAtributos(classe.getDeclaredFields());
			
			imprimirMetodos(classe.getDeclaredMethods());
			
			imprimirClasse(classe.getSuperclass());
		}
		return;
	}
	
	private void imprimirAtributos(Field[] fields) {
		System.out.println("====================");
		System.out.println("     ATRIBUTOS      ");
		System.out.println("====================");
		
		for (Field field : fields) {
			System.out.println(field.toString());
		}
	}
	
	private void imprimirMetodos(Method[] methods) {
		System.out.println("====================");
		System.out.println("       MÉTODOS      ");
		System.out.println("====================");
		
		for (Method method : methods) {
			System.out.println(method.toString());
		}
	}
}
