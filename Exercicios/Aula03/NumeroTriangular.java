import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NumeroTriangular {
	
	private void triangular(int numero) {
		int aux = 0;
		
		for(int i = 1; (i*(i+1)*(i+2)) < numero+1; i++) {
			if((i*(i+1)*(i+2)) == numero){
				aux = 0;
			}
			else{
				aux++;
			}
		}
		
		if(aux == 0) {
			System.out.println("O número informado é trianqular.");
		} else {
			System.out.println("O número informado não é trianqular.");
		}
	}
	
	private void solicitarNumero() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Informe um número inteiro: ");
        try{
            int numero = Integer.parseInt(br.readLine());
            
            if(numero == 0) {
            	System.err.println("O número não pode ser igual a 0.");
            	solicitarNumero();
            }
            
    		triangular(numero);
        }catch(NumberFormatException nfe){
            System.err.println("Formato inválido! Digite somente números.");
            solicitarNumero();
        }
	}
	
	public static void main(String[] args) {
		System.out.println("=================");
		System.out.println("NÚMERO TRIANGULAR");
		System.out.println("=================");
		
		NumeroTriangular numeroTriangular = new NumeroTriangular();
		
		try {
			numeroTriangular.solicitarNumero();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
