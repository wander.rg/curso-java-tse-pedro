import java.io.IOException;
import java.util.Scanner;

public class Quadrado {

	private void calcularQuadrados(int[] numeros) {
		for (int numero : numeros) {
			System.out.println(numero * numero);
		}
	}

	private void solicitarNumeros() throws IOException {
		System.out.println("Informe os numero separados por espaço ' ' e pressione 'ENTER' para continuar");

		boolean temNumero = false;

		Scanner in = new Scanner(System.in);
		int[] numeros = new int[5];

		System.out.printf("Informe %d numeros: ", numeros.length);
		for(int i = 0; i < numeros.length; i++) {
			if (in.hasNextInt()) {
	        	temNumero = true;
	        	numeros[i] = in.nextInt();
	        } else {
	            in.next();
	        }
		}

	    if(temNumero) {
	    	calcularQuadrados(numeros);
	    } else {
	    	System.out.println("Informe um ou mais números!");
	    	solicitarNumeros();
	    }
	}

	public static void main(String[] args) {
		System.out.println("====================");
		System.out.println("QUADRADO DOS NÚMEROS");
		System.out.println("====================");

		Quadrado quadrado = new Quadrado();

		try {
			quadrado.solicitarNumeros();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
