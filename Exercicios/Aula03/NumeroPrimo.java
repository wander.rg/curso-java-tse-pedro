import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NumeroPrimo {
	
	private void verificarNumeroPrimo(int numero) {
		int aux = 0;

		for (int i = 1; i <= numero; i++) {
			if (numero % i == 0) {
				aux++;
			}
		}

		if (aux == 2) {
			System.out.println("O número informado é primo");
		} else {
			System.out.println("O número informado não é primo");
		}
	}
	
	private void solicitarNumero() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Informe um número inteiro: ");
        try{
            int numero = Integer.parseInt(br.readLine());
            
            if(numero == 0) {
            	System.err.println("O número não pode ser igual a 0.");
            	solicitarNumero();
            }
            
    		verificarNumeroPrimo(numero);
        }catch(NumberFormatException nfe){
            System.err.println("Formato inválido! Digite somente números.");
            solicitarNumero();
        }
	}
	
	public static void main(String[] args) {
		System.out.println("============");
		System.out.println("NÚMERO PRIMO");
		System.out.println("============");
		
		NumeroPrimo numeroPrimo = new NumeroPrimo();
		
		try {
			numeroPrimo.solicitarNumero();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
