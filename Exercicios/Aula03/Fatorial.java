import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Fatorial {
	
	private void calcularFatorial(int numero) {
		int fatorial = numero;
		int n = numero;
		
	    if (numero == 0) {
	    	fatorial++;
	    }
	    
	    while (numero > 1) {
	    	fatorial *= --numero;
	    }
	    
	    System.out.printf("Fatorial do número %d: %d \n", n, fatorial);
	}
	
	private void solicitarNumero() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Informe um número inteiro: ");
        try{
            int numero = Integer.parseInt(br.readLine());
            
            if(numero == 0) {
            	System.err.println("O número não pode ser igual a 0.");
            	solicitarNumero();
            }
            
            calcularFatorial(numero);
        }catch(NumberFormatException nfe){
            System.err.println("Formato inválido! Digite somente números.");
            solicitarNumero();
        }
	}
	
	public static void main(String[] args) {
		System.out.println("========");
		System.out.println("FATORIAL");
		System.out.println("========");
		
		Fatorial fatorial = new Fatorial();
		
		try {
			fatorial.solicitarNumero();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
