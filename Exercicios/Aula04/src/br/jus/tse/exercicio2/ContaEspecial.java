package br.jus.tse.exercicio2;

public class ContaEspecial extends ContaCorrente {
	
	private static final double TAXA_SAQUE = 0.001; //0,1%

	public ContaEspecial(double valor) {
		super(valor, TAXA_SAQUE);
	}
}
