package br.jus.tse.exercicio2;

public class ContaCorrente {

	private double saldo;
	
	private double taxa;

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public double getTaxa() {
		return taxa;
	}

	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}

	public ContaCorrente(double valor, double taxa) {
		this.saldo = valor;
		this.taxa = taxa;
	}
	
	public void depositar(double valor) {
		setSaldo(getSaldo() + valor);
	}
	
	public double sacar(double valor) {
		double valorTaxa = valor * getTaxa();
		double valorTotalSaque = valor + valorTaxa;
		
		if(getSaldo() > valorTotalSaque) {
			setSaldo(getSaldo() - valorTotalSaque);
			return valorTotalSaque;
		}
		
		return 0;
	};
}
