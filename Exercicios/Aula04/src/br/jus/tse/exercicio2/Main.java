package br.jus.tse.exercicio2;

public class Main {
	
	public static void main(String[] args) {
		double valorInicial = 1000;
		double valorDeposito = 500;
		double valorSaque = 1400;
		
		ContaNormal contaNormal = new ContaNormal(valorInicial);
		
		System.out.println("=== CONTA NORMAL ===");
		System.out.printf("Saldo inicial: R$%.2f \n", contaNormal.getSaldo());
		
		contaNormal.depositar(valorDeposito);
		System.out.printf("Depositando: R$%.2f. Novo saldo: R$%.2f \n", valorDeposito, contaNormal.getSaldo());
		
		System.out.printf("Sacando: R$%.2f \n", valorSaque);
		
		if(contaNormal.sacar(valorSaque) > 0) {
			System.out.printf("Saque efetuado com sucesso! Saldo restante: R$%.2f \n", contaNormal.getSaldo());
		} else {
			System.out.printf("Saldo insuficiente! Saldo atual: R$%.2f \n", contaNormal.getSaldo());
		}
				
		ContaEspecial contaEspecial = new ContaEspecial(valorInicial);
		
		System.out.println("\n=== CONTA ESPECIAL ===");
		System.out.printf("Saldo inicial: R$%.2f \n", contaEspecial.getSaldo());
		
		contaEspecial.depositar(valorDeposito);
		System.out.printf("Depositando: R$%.2f. Novo saldo: R$%.2f \n", valorDeposito, contaEspecial.getSaldo());
		
		System.out.printf("Sacando: R$%.2f \n", valorSaque);
		
		if(contaEspecial.sacar(valorSaque) > 0) {
			System.out.printf("Saque efetuado com sucesso! Saldo restante: R$%.2f \n", contaEspecial.getSaldo());
		} else {
			System.out.printf("Saldo insuficiente! Saldo atual: R$%.2f \n", contaEspecial.getSaldo());
		}
	}
}
