package br.jus.tse.exercicio2;

public class ContaNormal extends ContaCorrente {

	private static final double TAXA_SAQUE = 0.005; //0,5%
	
	public ContaNormal(double valor) {
		super(valor, TAXA_SAQUE);
	}
}
