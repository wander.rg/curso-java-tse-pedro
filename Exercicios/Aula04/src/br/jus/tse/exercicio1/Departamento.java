package br.jus.tse.exercicio1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Departamento {
	
	private static final int MAX_FUNCIONARIOS   = 100;

	private Empresa empresa;
	
	private String nome;
	
	private List<Funcionario> funcionarios;

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	
	public void adicionarFuncionarios(int qtd) {
		List<Funcionario> funcionarios = new ArrayList<Funcionario>();
		
		if(qtd < MAX_FUNCIONARIOS) {
			for(int i = 1; i <= qtd; i++) {
				Funcionario funcionario = new Funcionario();
				funcionario.setDepartamento(this);
				funcionario.setNomeCompleto("Funcionário " + i);
				funcionario.setSalario(1000 * i);
				funcionario.setDataAdmissao(new Date());
				
				funcionarios.add(funcionario);
			}
			
			this.setFuncionarios(funcionarios);
		}
	}
	
	public void aumentoGeral(int aumento) {
		for (Funcionario funcionario : funcionarios) {
			funcionario.setSalario(funcionario.getSalario() + aumento);
		}
	}
	
}
