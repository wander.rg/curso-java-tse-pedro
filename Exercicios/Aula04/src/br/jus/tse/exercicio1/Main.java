package br.jus.tse.exercicio1;

public class Main {

	public static void main(String[] args) {
		System.out.println("==============================");
		System.out.println("Iniciando a criação da Empresa");
		System.out.println("==============================");
		
		Empresa empresa = new Empresa();
		
		String nome = "Empresa XPTO";
		empresa.setNome(nome);
		System.out.println("Nome: " + nome);
		
		String cnpj = "00.000.000/0000-00";
		empresa.setCnpj(cnpj);
		System.out.println("CNPJ: " + cnpj);
		
		System.out.println("==============================");
		System.out.println("  Adicionando departamentos   ");
		System.out.println("==============================");
		
		empresa.adicionarDepartamentos(7);
		
		System.out.println("==============================");
		System.out.println("     Aumento Geral R$500      ");
		System.out.println("==============================");
		
		empresa.getDepartamentos().get(5).aumentoGeral(500);
		
		System.out.println("==============================");
		System.out.println("   Transferindo funcionário   ");
		System.out.println("==============================");
		
		empresa.transferirFuncionario(2, 1);
	}
}
