package br.jus.tse.exercicio1;

import java.util.ArrayList;
import java.util.List;

public class Empresa {
	
	private static final int MAX_DEPARTAMENTOS = 10;

	private String nome;
	
	private String cnpj;
	
	private List<Departamento> departamentos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public List<Departamento> getDepartamentos() {
		return departamentos;
	}

	public void setDerpartamentos(List<Departamento> departamentos) {
		this.departamentos = departamentos;
	}
	
	public void adicionarDepartamentos(int qtd) {
		List<Departamento> departamentos = new ArrayList<Departamento>();
		
		int f = 10;
		
		if(qtd < MAX_DEPARTAMENTOS) {

			for(int i = 1; i <= qtd; i++) {
				Departamento departamento = new Departamento();
				departamento.setEmpresa(this);
				departamento.setNome("Departamento " + i);
				
				departamento.adicionarFuncionarios(f);
				
				departamentos.add(departamento);
			}
			
			this.setDerpartamentos(departamentos);
		}
		
		System.out.println("Departamentos adicionados: " + qtd);
		System.out.println("Funcionários adicionados por departamento: " + f);
	}
	
	public void transferirFuncionario(int depAtual, int novoDep) {
		int iTranferir = 0;
		
		Departamento dAtual = getDepartamentos().get(depAtual);
		
		Funcionario fTransferir = dAtual.getFuncionarios().get(iTranferir);
		
		Departamento novoDepartamento = getDepartamentos().get(novoDep);
		novoDepartamento.getFuncionarios().add(fTransferir);
		
		getDepartamentos().get(depAtual).getFuncionarios().remove(iTranferir);
		
		System.out.println("Tranferiu o funcionário: " + fTransferir.getNomeCompleto());
		System.out.println("Do departamento: " + dAtual.getNome());
		System.out.println("Para o departamento: " + novoDepartamento.getNome());
	}
	
}
